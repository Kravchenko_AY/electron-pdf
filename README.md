# README #


### Для начала ###
* выполнить в терминале команду - npm install
* положить проект в папку build

### Как запустить просмотр и отладку проекта? ###

* выполнить в терминале команду - npm start

### Как сделать иконки? ###

* выполнить в терминале команду - npm install --save-dev electron-icon-maker
* поместить исходный файл иконки - ./icon-input/logo.png
* выполнить в терминале команду - ./node_modules/.bin/electron-icon-maker --input=./icon-input/logo.png --output=./icon-output

### Как изменить размеры запускаемого окна? ###

* поменять значения в основном файле index.js электрона в функции createWindow

### Как собрать релиз проекта для Windows? ###

* выполнить в терминале команду - electron-packager . install --overwrite --platform=win32 --arch=ia32 --icon=assets/icons/win/icon.ico --prune=true --out=release --version-string.ProductName="yourProjectName"

### Как собрать релиз проекта для Mac? ###

* выполнить в терминале команду - electron-packager . install --overwrite --platform=darwin --icon=icon-output/icons/mac/icon.icns --prune=true --out=release --version-string.ProductName="yourProjectName"