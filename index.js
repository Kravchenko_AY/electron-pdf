const electron = require('electron');
const path = require('path');
const PDFWindow = require('electron-pdf-window');


const app = electron.app;
const BrowserWindow = electron.BrowserWindow;

let mainWindow;

function createWindow () {
  mainWindow = new BrowserWindow({
    width: 1024, 
    height: 800,
    center: true,
    icon: path.join(__dirname, 'icon-output/icons/png/64x64.png'),
    webPreferences: {
      plugins: true,
      allowRunningInsecureContent: true,
      webSecurity: false,
      experimentalFeatures: true
    }
  });

  mainWindow.loadURL(`file://${__dirname}/build/index.html`);
  mainWindow.webContents.openDevTools();
  mainWindow.on('closed', function () {
    mainWindow = null;
  });
}

app.on('ready', createWindow);

app.on('window-all-closed', function () {
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

app.on('activate', function () {
  if (mainWindow === null) {
    createWindow();
  }
});

global.openPDF = function (file_url) {
  const win = new PDFWindow({
    width: 1024,
    height: 800
  }); 
  win.loadURL(`file://${__dirname}/build/`+file_url);
}
